<?php
require_once("./data/data.php");
$DATA = load();
?>

<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.4.0.0.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="/assets/css/style.css">

    <title>Hello, world!</title>
</head>
<body>
<div class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class="col-12 mx-auto">
            <div class="jumbotron">
                <h1 class="display-4">Willkommen auf der Homepage von <?php echo $DATA['name']; ?>!</h1>
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" id="steckbrief-tab" data-toggle="tab"
                                   aria-controls="steckbrief"
                                   aria-selected="true" href="#steckbrief">Das bin ich</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="vergangenheit-tab" data-toggle="tab"
                                   aria-controls="vergangenheit"
                                   aria-selected="false" href="#vergangenheit">Meine Vergangenheit</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="likes-tab" data-toggle="tab" aria-controls="likes"
                                   aria-selected="false" href="#likes">Was ich mag</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content card-body">
                        <div class="tab-pane fade show active" id="steckbrief" role="tabpanel"
                             aria-labelledby="steckbrief-tab">
                            <h2 class="card-title">Mein Steckbrief:</h2>
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <img class="col-12" src="/assets/img/profile.jpg" class="rounded float-left"
                                         alt="Profil Bild">
                                </div>
                                <div class="col-12 col-md-8">
                                    <div class="container">
                                        <div class="row">
                                            <p></p>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">Name</div>
                                            <div class="col-9">
                                                <span itemprop="name"><?php echo $DATA['name']; ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">Geburtsdatum</div>
                                            <div class="col-9">
                                                <span itemprop="birthday"
                                                      datetime="<?php echo $DATA['date']; ?>"><?php echo $DATA['date']; ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">Ort</div>
                                            <div class="col-9">
                                                <span itemprop="addressLocality"><?php echo $DATA['location']; ?></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">...</div>
                                            <div class="col-9">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="vergangenheit" role="tabpanel"
                             aria-labelledby="vergangenheit-tab">
                            <h2 class="card-title">Es war einmal:</h2>
                        </div>
                        <div class="tab-pane fade" id="likes" role="tabpanel"
                             aria-labelledby="likes-tab">
                            <h2 class="card-title">I Like:</h2>
                        </div>
                    </div>
                </div>
                <br/>
                <p class="lead" style="margin: 0">
                    <a href="/admin" class="btn btn-primary btn-sm" role="button">Angaben ändern</a>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/assets/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="/assets/js/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="/assets/js/bootstrap.4.0.0.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>