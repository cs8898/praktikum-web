<?php
session_start();
$err = false;
if (isset($_POST['password'])) {
    if ($_POST['password'] === "secret") {
        $_SESSION['login'] = true;
        header("Location: /admin");
        return;
    } else {
        $err = true;
    }
}
$_SESSION['login'] = false;
?>

<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.4.0.0.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="/assets/css/style.css">

    <title>Hello, world!</title>
</head>
<body>
<div class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class="col-12 mx-auto">
            <div class="alert alert-danger" <?php if (!$err) {
                echo "hidden";
            } ?>>
                Falsches Passwort!
            </div>
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title">Login</h1>
                    <h6 class="card-subtitle mb-2 text-muted">Bearbeiten der Daten von Christian Schmied</h6>
                </div>
                <form method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="passwordInput">Password</label>
                            <input type="password" name="password" class="form-control" id="passwordInput"
                                   placeholder="Password">
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/assets/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="/assets/js/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="/assets/js/bootstrap.4.0.0.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>