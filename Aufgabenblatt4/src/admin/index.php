<?php
session_start();
if (!$_SESSION['login']) {
    header("Location: /admin/login.php");
    return;
}

require_once("../data/data.php");

$oldData = load();

if (isset($_POST['action'])) {
    if ($_POST['action'] === 'save') {
        foreach ($oldData as $key => $value){
            if(isset($_POST[$key])){
                $oldData[$key] = $_POST[$key];
            }
        }
        save($oldData);
    }
    header("Location: /");
    return;
}
?>

<!doctype html>
<html lang="de">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.4.0.0.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="/assets/css/style.css">

    <title>Hello, world!</title>
</head>
<body>
<div class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class="col-12 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title">Daten bearbeiten</h1>
                </div>
                <form method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nameInput">Name</label>
                            <input type="text" name="name" class="form-control" id="nameInput"
                                   value="<?php echo $oldData['name']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="dateInput">Datum</label>
                            <input type="date" name="date" class="form-control" id="dateInput" value="<?php echo $oldData['date']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="locationInput">Ort</label>
                            <input type="text" name="location" class="form-control" id="locationInput"
                                   value="<?php echo $oldData['location']; ?>">
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" name="action" value="save" class="btn btn-primary">Speichern</button>
                        <button type="submit" name="action" value="abort" class="btn btn-danger">Abbrechen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/assets/js/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="/assets/js/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="/assets/js/bootstrap.4.0.0.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>
</html>