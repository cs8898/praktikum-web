<?php
function load()
{
    $string = file_get_contents(dirname(__FILE__) . "/data.json");
    return json_decode($string, true);
}

function save($data)
{
    $json = json_encode($data, JSON_PRETTY_PRINT);
    file_put_contents(dirname(__FILE__) . "/data.json", $json);
}

?>
