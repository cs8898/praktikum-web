import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {Router} from '@angular/router';
import {Form} from '@angular/forms';
import {UrlaubsantragService} from 'src/app/services/UrlaubsantragService.service';
import {Urlaubsantrag} from 'src/app/models/Urlaubsantrag';
import * as moment from 'moment';
import {LoginService} from 'src/app/services/LoginService.service';
import {Benutzer} from 'src/app/models/Benutzer';
import {Mitarbeiter} from 'src/app/models/Mitarbeiter';

@Component({
    selector: 'app-antraege',
    templateUrl: './antraege.component.html',
    styleUrls: ['./antraege.component.css']
})
export class AntraegeComponent implements OnInit {
    public aktuellerAntrag: Urlaubsantrag;
    public bemerkung: string;
    public benutzer: Benutzer;
    public mitarbeiter: Mitarbeiter;
    public ueberstundentage: number;
    public urlaubstageNehmen: number;
    public ueberstundentageNehmen: number;


    /* Modal Bindings */
    public newAntragVon: string;
    public newAntragBis: string;
    public ablehnBemerkung: string;

    private ablehnenId: number;

    public constructor(private router: Router, private speicherService: UrlaubsantragService,
                       private loginService: LoginService) {
        this.mitarbeiter = this.loginService.getAktuellerMitarbeiter();
        this.aktuellerAntrag = null;
        this.bemerkung = '';
    }

    public ngOnInit(): void {
        this.benutzer = this.loginService.getAktuellerBenutzer();
        if (this.benutzer == null) {
            this.router.navigate(['login']);
        }
    }

    public schliessen(): void {
        this.router.navigate(['login']);
    }

    /**
     * Berechnet die Anzahl Werktage zwischen von- und bis-Datum.
     * @param von starting Date String
     * @param bis ending Date String
     */
    public tage(von: string, bis: string): number {
        // Hartes Convertieren auf Datum
        const vonMoment = moment(moment(von).format('YYYY-MM-DD'));
        const bisMoment = moment(moment(bis).format('YYYY-MM-DD'));

        // Eventuelle Fehler Behandlung
        if (bisMoment.isBefore(vonMoment)) {
            return -1;
        }
        let urlaubstage = 0;

        while (true) {
            const day = vonMoment.weekday();
            if (day > 0 && day < 6) { // 0 = Sonntag, 6 = Samstag
                urlaubstage++;
            }
            if (vonMoment.isSame(bisMoment)) {
                break;
            }
            vonMoment.add(1, 'days');
        }

        return urlaubstage;
    }

    /**
     * Schliesst den Dialog zum Stellen eines Antrags.
     */
    public antragStellenDialogSchliessen(): void {
        document.getElementById('antrag-stellen-dialog-schliessen').click();
    }

    /**
     * Schliesst den Dialog zum Stellen eines Antrags.
     */
    public antragAblehnenDialogSchliessen(): void {
        document.getElementById('antrag-ablehnen-dialog-schliessen').click();
    }

    public antraege(status: string): Array<Urlaubsantrag> {
        const result = Array<Urlaubsantrag>();

        if (this.mitarbeiter != null && this.mitarbeiter.urlaubsantraege != null) {
            this.mitarbeiter.urlaubsantraege.forEach(antrag => {
                if (antrag.status === status || status === '') {
                    result.push(antrag);
                }
            });
            result.sort((a1, a2) => {
                const m1 = moment(a1.von);
                const m2 = moment(a2.von);

                if (m1.isSame(m2)) {
                    return 0;
                }
                if (m1.isBefore(moment(m2))) {
                    return 1;
                }
                return -1;
            });
        }

        return result;
    }

    public antraegeMitarbeiter(status: string): Array<Urlaubsantrag> {
        const result = Array<Urlaubsantrag>();

        if (this.mitarbeiter != null && this.mitarbeiter.urlaubsantraegeMitarbeiter != null) {
            this.mitarbeiter.urlaubsantraegeMitarbeiter.forEach(antrag => {
                if (antrag.status === status || status === '') {
                    result.push(antrag);
                }
            });
            result.sort((a1, a2) => {
                // Zahlen sind leicher im sortieren ;D
                const bearbeitetA1: number = a1.status !== 'unbearbeitet' ? 0 : 1;
                const bearbeitetA2: number = a2.status !== 'unbearbeitet' ? 0 : 1;

                if (bearbeitetA2 - bearbeitetA1 !== 0) {
                    return bearbeitetA2 - bearbeitetA1;
                }

                const m1 = moment(a1.zeitstempel);
                const m2 = moment(a2.zeitstempel);

                if (m1.isSame(m2)) {
                    return 0;
                }
                if (m1.isBefore(moment(m2))) {
                    return -1;
                }
                return 1;
            });
        }

        return result;
    }

    public antragStellen(form: Form): void {
        // console.log('NeuerAntrag Von: ', this.newAntragVon);
        // console.log('NeuerAntrag Bis: ', this.newAntragBis);
        const antrag: Urlaubsantrag = new Urlaubsantrag(
            -1, this.newAntragVon, this.newAntragBis,
            this.mitarbeiter.id, this.mitarbeiter.name,
            new Date().toLocaleString(), 'unbearbeitet', '');
        // Die Letzten 3 Werte sollten automatisch vom Dienst gesetztwerden
        // Diese sollte der Client nicht selbst setzen Dürfen!
        this.speicherService.speichereUrlaubsantrag(antrag)
            .then(id => {
                // Eigentlich schöner, ein JSON mit vollem Objekt zurückgeben!
                // Nur mit ID müsste der Antrag eig nochmals vom Server geholt werden.
                // Antrag mit ID erstellt
                // console.log('id: ' + id);
                antrag.id = id;
                // Antrag in Liste Aufnehmen mit neuer ID;
                this.mitarbeiter.urlaubsantraege.push(antrag);
            })
            .catch(() => {
                alert('Stellen des Antrags Fehlgeschlagen');
            });
    }

    public antragLoeschen(id: number): void {
        this.speicherService.loescheUrlaubsantrag(id)
            .catch(() => {
                alert('Löschen des Antrags Fehlgeschlagen!');
            })
            .then(() => {
                this.deleteAntragFromList(id);
            });
    }

    antragMitarbeiterLoeschen(id: number): void {
        this.speicherService.loescheUrlaubsantrag(id)
            .catch(() => {
                alert('Löschen des Mitarbeiter Antrags Fehlgeschlage');
            })
            .then(() => {
                this.deleteMitarbeiterAntragFromList(id);
            });
    }

    // Helper Function to Delete By ID
    public deleteAntragFromList(id: number): void {
        const oldIndex = this.mitarbeiter.urlaubsantraege.findIndex((obj) => {
            return obj.id === id;
        });
        this.mitarbeiter.urlaubsantraege.splice(oldIndex, 1);
    }

    public deleteMitarbeiterAntragFromList(id: number): void {
        const oldIndex = this.mitarbeiter.urlaubsantraegeMitarbeiter.findIndex((obj) => {
            return obj.id === id;
        });
        this.mitarbeiter.urlaubsantraegeMitarbeiter.splice(oldIndex, 1);
    }


    public getMitarbeiterAntragFromList(id: number): Urlaubsantrag | undefined {
        return this.mitarbeiter.urlaubsantraegeMitarbeiter.find((obj) => {
            return obj.id === id;
        });
    }

    public genehmigen(id: number): void {
        const antrag = this.getMitarbeiterAntragFromList(id);
        if (antrag !== undefined) {
            const statusOld = antrag.status;
            antrag.status = 'genehmigt';
            this.speicherService.aktualisiereUrlaubsantrag(antrag)
                .catch(() => {
                    alert(`Genehmigen Fehlgeschlagen...\nPech für ${antrag.antragsteller}!`);
                    antrag.status = statusOld;
                });
        }
    }

    // Ablehn Dialog Öffnen, incl Bootstrap Animation ;D
    public ablehnen(id: number): void {
        this.ablehnenId = id;
        this.ablehnBemerkung = '';
        document.getElementById('antrag-ablehnen-dialog-button').click();
    }

    public ablehnenSubmit(): void {
        const antrag = this.getMitarbeiterAntragFromList(this.ablehnenId);
        if (antrag !== undefined) {
            const statusOld = antrag.status;
            antrag.status = 'abgelehnt';
            antrag.bemerkung = this.ablehnBemerkung;
            this.speicherService.aktualisiereUrlaubsantrag(antrag)
                .then(() => {
                    this.antragAblehnenDialogSchliessen();
                })
                .catch((err) => {
                    console.log(err);
                    alert(`Ablehnen Fehlgeschlagen...\nGlück für ${antrag.antragsteller}!`);
                    antrag.status = statusOld;
                    antrag.bemerkung = '';
                });
        }
    }

}
