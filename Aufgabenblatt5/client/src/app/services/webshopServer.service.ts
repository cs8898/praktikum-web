import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Benutzer} from '../models/Benutzer';
import {Warenkorb} from '../models/Warenkorb';
import {Kunde} from '../models/Kunde';
import {Artikel} from '../models/Artikel';

@Injectable()
/**
 * Schnittstellenservice für den Webshop-Server. Dieser stellt Operationen für Login, Artikeldaten und Warenkorbdaten zur Verfügung.
 *
 * Diese Implementierung simuliert den Server, indem Arrays mit Kunden, Artikel und Warenkörbe gehalten werden.
 * Die Operationen des Service greifen auf die Arrays zu.
 */
export class WebshopServer {
  /*private kunden: Array<Kunde> = JSON.parse(
    `[
        {
            "id": 1,
            "name": "Hugo Maier",
            "benutzername": "Hugo",
            "passwort": "123"
        },
        {
            "id": 2,
            "name": "Codie Coder",
            "benutzername": "Codie",
            "passwort": "123"
        }
    ]`);
  private artikel: Array<Artikel> = JSON.parse(
    `[
        {
            "id": 1,
            "kurzText": "Milch",
            "beschreibung": "1L H-Milch 3,5% Fett",
            "preis": "0.80"
        },
        {
            "id": 2,
            "kurzText": "Butter",
            "beschreibung": "250gr. Markenbutter",
            "preis": "1.80"
        },
        {
            "id": 3,
            "kurzText": "Essig",
            "beschreibung": "1L Balsamico-Essig",
            "preis": "2.80"
        }
    ]`);
  private warenkoerbe: Array<Warenkorb> = JSON.parse(
    `{
        "1": {
            "id": 1,
            "kundenId": 1,
            "status": "angelegt",
            "positionen": [
                {
                    "nr": 1,
                    "artikelId": 2,
                    "menge": 1
                },
                {
                    "nr": 2,
                    "artikelId": 1,
                    "menge": 8
                },
                {
                    "nr": 3,
                    "artikelId": 3,
                    "menge": 9
                }
            ]
        },
        "2": {
            "id": 2,
            "kundenId": 2,
            "status": "angelegt",
            "positionen": []
        }
    }`);*/

  public aktuellerKunde: Kunde;

  constructor(private http: HttpClient) {
  }

  public async login(benutzer: Benutzer): Promise<Kunde | null> {
    try {
      const res: any = await this.http.post('/rest/benutzer/login', benutzer).toPromise();
      this.aktuellerKunde = new Kunde(res.id, res.name, res.benutzername, res.passwort);
      return this.aktuellerKunde;
    } catch (err) {
      console.error(err);
    }
    return null;
  }

  public async ladeWarenkorbZuKunde(kundeId: number): Promise<Warenkorb> {
    const res: any = await this.http.get(`/rest/kunde/${kundeId}/warenkorb?status=angelegt`).toPromise();
    return res;
  }

  public async ladeAlleArtikel(): Promise<Array<Artikel>> {
    const res: any = await this.http.get('/rest/artikel').toPromise();
    const resultList: Array<Artikel> = new Array<Artikel>();
    res.forEach(entry => {
      resultList.push(new Artikel(entry.id, entry.kurzText, entry.beschreibung, entry.preis));
    });

    return resultList;
  }

  public async speichereWarenkorb(warenkorb: Warenkorb): Promise<void> {
    const res: any = await this.http.put(`/rest/kunde/${warenkorb.kundenId}/warenkorb/${warenkorb.id}`, warenkorb).toPromise();
    if (res.status === 'bezahlt') {
      const newBasket = new Warenkorb(0, warenkorb.kundenId, 'angelegt', []);
      await this.http.post(`/rest/kunde/${newBasket.kundenId}/warenkorb`, newBasket).toPromise();
    }
  }
}
