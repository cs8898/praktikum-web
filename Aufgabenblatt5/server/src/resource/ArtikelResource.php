<?php declare(strict_types=1);

namespace Web\A5\Resource;
require_once __DIR__ . "/../dao/ArtikelDAO.php";

use Web\A5\Dao\ArtikelDAO;
use Web\A5\Request\RequestContext;

require_once __DIR__ . "/AbstractResource.php";

class ArtikelResource extends AbstractResource
{
    private $artikelDAO;

    public function __construct()
    {
        $this->_GET_P = [
            "/artikel" => "getArtikel",
        ];
        $this->artikelDAO = new ArtikelDAO();
    }

    public function getArtikel(RequestContext $context)
    {
        return [$this->artikelDAO->getAll(), 200];
    }
}