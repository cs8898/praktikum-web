<?php declare(strict_types=1);

namespace Web\A5\Resource;
require_once __DIR__ . "/../dto/BenutzerDTO.php";
require_once __DIR__ . "/../dao/KundenDAO.php";

use Web\A5\Dao\KundenDAO;
use Web\A5\Dto\BenutzerDTO;
use Web\A5\Request\RequestContext;

require_once __DIR__ . "/AbstractResource.php";

class BenutzerResource extends AbstractResource
{
    private $kundenDAO;

    public function __construct()
    {
        $this->_POST_P = [
            "/benutzer/login" => "login",
        ];
        $this->kundenDAO = new KundenDAO();
    }

    public function login(RequestContext $context)
    {
        $benutzerDto = new BenutzerDTO();
        $benutzerDto->map($context->_BODY);

        $foundUser = $this->kundenDAO->checkLogin($benutzerDto);
        if ($foundUser != null) {
            return [$foundUser, 200];
        } else {
            return 404;
        }
    }
}