<?php


namespace Web\A5\Resource;


abstract class AbstractResource
{
    protected $_GET_P = [];
    protected $_POST_P = [];
    protected $_PUT_P = [];
    protected $_DELETE_P = [];

    public function _GET_PATTERNS(): array {
        return $this->_GET_P;
    }
    public function _POST_PATTERNS(): array {
        return $this->_POST_P;
    }
    public function _PUT_PATTERNS(): array {
        return $this->_PUT_P;
    }
    public function _DELETE_PATTERNS(): array {
        return $this->_DELETE_P;
    }

    //public abstract function handle(string $url, string $pathParams, array $queryParams, string $postBody );

}