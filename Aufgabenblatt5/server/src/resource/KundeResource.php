<?php declare(strict_types=1);

namespace Web\A5\Resource;
require_once __DIR__ . "/../dao/WarenkorbDAO.php";
require_once __DIR__ . "/../dao/KundenDAO.php";
require_once __DIR__ . "/../dto/WarenkorbDTO.php";

use Web\A5\Dao\KundenDAO;
use Web\A5\Dao\WarenkorbDAO;
use Web\A5\Dto\WarenkorbDTO;
use Web\A5\Request\RequestContext;

require_once __DIR__ . "/AbstractResource.php";

class KundeResource extends AbstractResource
{
    private $kundenDAO;
    /**
     * @var WarenkorbDAO
     */
    private $warenkorbDAO;

    public function __construct()
    {
        $this->_GET_P = [
            "/kunde/{kid}/warenkorb" => "getWarenKorb",
        ];
        $this->_POST_P = [
            "/kunde/{kid}/warenkorb" => "newWarenkorb",
        ];
        $this->_PUT_P = [
            "/kunde/{kid}/warenkorb/{wid}" => "updateWarenkorb"
        ];
        $this->kundenDAO = new KundenDAO();
        $this->warenkorbDAO = new WarenkorbDAO();
    }

    public function getWarenKorb(RequestContext $context)
    {
        $foundBasket = $this->warenkorbDAO->findByKundenIdWhereState(
            intval($context->_ARGS['{kid}']),
            $context->_QUERY['status']
        );
        if ($foundBasket != null) {
            return [$foundBasket, 200];
        } else {
            return 404;
        }
    }

    /**
     * Erstellt neuen Warenkorb oder gibt alten zurück
     * @param RequestContext $context
     * @return array
     */
    public function newWarenkorb(RequestContext $context)
    {
        $foundBasket = $this->warenkorbDAO->findByKundenIdWhereState(
            intval($context->_ARGS['{kid}']),
            "angelegt"
        );
        if ($foundBasket == null) {
            $newWarenkorb = new WarenkorbDTO();
            $newWarenkorb->kundenId = intval($context->_ARGS['{kid}']);
            $newWarenkorb->status = "angelegt";
            $newWarenkorb->positionen = [];
            $saved = $this->warenkorbDAO->save((array) $newWarenkorb);
            return [$saved, 201];
        } else {
            return [$foundBasket, 200];
        }
    }

    /**
     * Updated alten Warenkorb
     * @param RequestContext $context
     * @return array|int
     */
    public function updateWarenkorb(RequestContext $context)
    {
        $foundBasket = $this->warenkorbDAO->findByid(
            intval($context->_ARGS['{wid}'])
        );
        if ($foundBasket == null) {
            return ['{"error":"OldBasket Not Found"}', 404];
        } else {
            return [$this->warenkorbDAO->save($context->_BODY), 200];
        }
    }
}