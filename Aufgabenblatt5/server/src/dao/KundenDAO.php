<?php declare(strict_types=1);

namespace Web\A5\Dao;

use Web\A5\Dto\BenutzerDTO;

require_once __DIR__ . "/AbstractDAO.php";

class KundenDAO extends AbstractDAO
{
    public function __construct()
    {
        $this->_FILE_NAME = "kunden";
        parent::__construct();
    }

    public function checkLogin(BenutzerDTO $benutzer)
    {
        foreach ($this->_DATA as $key => $value) {
            if (strcmp($value['benutzername'], $benutzer->name) == 0 &&
                strcmp($value['passwort'], $benutzer->passwort) == 0) {
                return $value;
            }
        }
        return null;
    }
}
