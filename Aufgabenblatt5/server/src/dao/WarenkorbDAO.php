<?php declare(strict_types=1);

namespace Web\A5\Dao;

use Web\A5\Dto\BenutzerDTO;

require_once __DIR__ . "/AbstractDAO.php";

class WarenkorbDAO extends AbstractDAO
{
    public function __construct()
    {
        $this->_FILE_NAME = "warenkorb";
        parent::__construct();
    }

    public function findByKundenIdWhereState(int $kundenid, string $status)
    {
        foreach ($this->_DATA as $key => $value) {
            if ($value['kundenId'] == $kundenid && strcmp($value['status'], $status) == 0) {
                return $value;
            }
        }
        return null;
    }
}
