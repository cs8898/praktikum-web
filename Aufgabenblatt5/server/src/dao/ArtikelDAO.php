<?php declare(strict_types=1);

namespace Web\A5\Dao;

require_once __DIR__ . "/AbstractDAO.php";

class ArtikelDAO extends AbstractDAO
{
    public function __construct()
    {
        $this->_FILE_NAME = "artikel";
        parent::__construct();
    }

}
