<?php declare(strict_types=1);

namespace Web\A5\Dao;


abstract class AbstractDAO
{
    private $_filePath;
    protected $_FILE_NAME;
    protected $_DATA;
    private $modification;

    public function __construct()
    {
        $this->_filePath = __DIR__ . "/../data/" . $this->_FILE_NAME . ".json";
        $this->loadData();
    }

    public function __destruct()
    {
        $this->writeData();
    }

    protected function loadData()
    {
        $this->_DATA = json_decode(file_get_contents($this->_filePath), true);
    }

    protected function writeData()
    {
        if ($this->modification == true) {
            file_put_contents($this->_filePath, json_encode($this->_DATA, JSON_PRETTY_PRINT));
        }
    }

    public function getAll()
    {
        return $this->_DATA;
    }

    public function findById(int $id)
    {
        foreach ($this->_DATA as $key => $value) {
            if ($value['id'] == $id) {
                return $value;
            }
        }
        return null;
    }

    public function save($obj)
    {
        if ($obj['id'] == null || $obj['id'] <= 0) {
            // NEW
            $max = 0;
            foreach ($this->_DATA as $key => $value) {
                if ($max < $value['id']) {
                    $max = $value['id'];
                }
            }
            $obj['id'] = $max + 1;
            array_push($this->_DATA, $obj);
        } else {
            // UPDATE
            foreach ($this->_DATA as $key => $value) {
                if ($value['id'] == $obj['id']) {
                    $this->_DATA[$key] = $obj;
                    break;
                }
            }
        }
        $this->modification = true;
        return $obj;
    }
}