<?php


namespace Web\A5\Request;

function startsWith($haystack, $needle)
{
    $length = strlen($needle);
    return substr($haystack, 0, $length) === $needle;
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if (!$length) {
        return true;
    }
    return substr($haystack, -$length) === $needle;
}


class RequestContext
{
    public $_METHOD;
    public $_PATH;
    public $_QUERY;
    public $_BODY;
    public $_ARGS;

    public function __construct()
    {
        $this->_METHOD = $_SERVER['REQUEST_METHOD'];
        $parsed = parse_url($_SERVER['REQUEST_URI']);
        $this->_PATH = $parsed['path'];
        if (strlen($this->_PATH) > 1 && endsWith($this->_PATH, "/")) {
            $this->_PATH = rtrim($this->_PATH, "/");
        }
        parse_str($parsed['query'], $this->_QUERY);

        //Support also Form Submits...
        $this->_BODY = $_POST;
        if (sizeof($this->_BODY) == 0) {
            $this->_BODY = json_decode(file_get_contents('php://input'), true);
        }
    }

    public function matchPaths(array $path): ?string
    {
        $res = false;
        foreach ($path as $key => $value) {
            $res = $this->matchPath($key);
            if ($res) {
                return $key;
            }
        }
        return null;
    }

    public function matchPath(string $path): bool
    {

        $params = [];

        $pattern = explode("/", $path);
        $url = explode("/", $this->_PATH);

        $valid = false;
        if (sizeof($pattern) == sizeof($url)) {
            // Same Count of Entrys
            $valid = true;
            for ($i = 0; $i < sizeof($pattern); $i++) {
                $is_param = startsWith($pattern[$i], "{") && endsWith($pattern[$i], "}");
                if ($is_param) {
                    $params[$pattern[$i]] = $url[$i];
                }
                if ($url[$i] != $pattern[$i] && !$is_param) {
                    $valid = false;
                    break;
                }
            }
        }
        if ($valid) {
            $this->_ARGS=$params;
        }
        return $valid;
    }
}