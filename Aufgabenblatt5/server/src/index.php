<?php declare(strict_types=1);

namespace Web\A5;

require_once "request/RequestContext.php";
require_once "resource/BenutzerResource.php";
require_once "resource/ArtikelResource.php";
require_once "resource/KundeResource.php";

use Web\A5\Request\RequestContext;
use Web\A5\Resource\ArtikelResource;
use Web\A5\Resource\BenutzerResource;
use Web\A5\Resource\KundeResource;

$_RESOURCES = [
    new BenutzerResource(),
    new ArtikelResource(),
    new KundeResource()
];

$requestContext = new RequestContext();

$matched_resource = null;
$matched_resource_method = null;

$matched_key = null;
foreach ($_RESOURCES as $key => $resource) {
    $_patterns = null;
    switch ($requestContext->_METHOD) {
        case "POST":
            $_patterns = $resource->_POST_PATTERNS();
            break;
        case "GET":
            $_patterns = $resource->_GET_PATTERNS();
            break;
        case "PUT":
            $_patterns = $resource->_PUT_PATTERNS();
            break;
        case "DELETE":
            $_patterns = $resource->_DELETE_PATTERNS();
            break;
        default:
            return 405;
    }

    $foundKey = $requestContext->matchPaths($_patterns);
    if ($foundKey != null) {
        // Matching Path Found
        $matched_resource = $resource;
        $matched_resource_method = $_patterns[$foundKey];
        $matched_key = $foundKey;
        break;
    }
    //Try Next One
}

header("Content-Type: application/json");

//print_r($matched_resource);
//print_r($requestContext);
if ($matched_resource != null && $matched_resource_method != null) {
    $res = $matched_resource->{$matched_resource_method}($requestContext);
    if (is_array($res)) {
        if (isset($res[1]) && is_numeric($res[1])) {
            http_response_code($res[1]);
        }
        if (is_string($res[0])) {
            echo $res[0];
        } else {
            echo json_encode($res[0]);
        }
    } elseif (is_string($res)) {
        echo $res;
    } elseif (is_numeric($res)) {
        http_response_code($res);
    } elseif ($res != null) {
        echo json_encode($res);
    } else {
        http_response_code(500);
        echo '{"error": "no response submitted"}';
    }
} else {
    http_response_code(501);
    echo '{"error": "not implemented"}';
}
