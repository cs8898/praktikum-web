<?php declare(strict_types=1);
namespace Web\A5\Dto;
require_once "AbstractDTO.php";

class WarenkorbDTO extends AbstractDTO
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $kundenId;
    /**
     * @var string
     */
    public $status;
    /**
     * @var WarenkorbPositionDTO[]
     */
    public $positionen;
}