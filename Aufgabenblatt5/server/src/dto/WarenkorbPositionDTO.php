<?php declare(strict_types=1);
namespace Web\A5\Dto;
require_once "AbstractDTO.php";

class WarenkorbPositionDTO extends AbstractDTO
{
    /**
     * @var integer
     */
    public $nr;
    /**
     * @var integer
     */
    public $artikelId;
    /**
     * @var string
     */
    public $artikelKurzText;
    /**
     * @var double
     */
    public $artikelPreis;
    /**
     * @var integer
     */
    public $menge;
    /**
     * @var integer
     */
    public $mengeNeu;
}