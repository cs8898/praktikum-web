<?php declare(strict_types=1);
namespace Web\A5\Dto;
require_once "AbstractDTO.php";

class BenutzerDTO extends AbstractDTO
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $passwort;
}