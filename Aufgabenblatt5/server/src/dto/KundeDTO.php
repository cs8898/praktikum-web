<?php declare(strict_types=1);
namespace Web\A5\Dto;
require_once "AbstractDTO.php";

class KundeDTO extends AbstractDTO
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $benutzername;
    /**
     * @var string
     */
    public $passwort;
}