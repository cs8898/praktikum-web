<?php declare(strict_types=1);
namespace Web\A5\Dto;
require_once "AbstractDTO.php";

class ArtikelDTO extends AbstractDTO
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $kurzText;
    /**
     * @var string
     */
    public $beschreibung;
    /**
     * @var double
     */
    public $preis;
}