| Method | Path                                   | Body         | Result                               |
|--------|----------------------------------------|--------------|--------------------------------------|
| GET    | /artikel                               | NULL         | ArtikelDTO[] 200                     |
| POST   | /benutzer/login                        | BenutzerDTO  | KundeDTO 200; NULL 404               |
| GET    | /kunde/{kid}/warenkorb?status={status} | NULL         | WarenkorbDTO 200; NULL 404           |
| POST   | /kunde/{kid}/warenkorb                 | NULL         | WarenkorbDTO 201; WarenkorbDTO 201   |
| PUT    | /kunde/{kid}/warenkorb/{wid}           | WarenkorbDTO | WarenkorbDTO 200; NULL 404           |